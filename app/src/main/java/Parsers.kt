import helpers.DateHelper
import helpers.PlantHelper
import android.util.Log
import org.jetbrains.anko.db.RowParser
import java.util.*

/**
 * Created by wilhelm on 2017-10-30.
 */

class PlantParser : RowParser<Plant> {
    override fun parseRow(columns: Array<Any?>): Plant {
        var idLong = 1.toLong()
        try {
            idLong = columns[0] as Long
        } catch (ex: Exception) {
            Log.d("Error", "No id for ${columns[1]}")
        }
        val waterInterval = columns[3] as Long
        val nurtureInterval = columns[4] as Long
        val imagePath = columns[2] as String?

        return Plant(idLong.toInt(),
                columns[1] as String,
                imagePath,
                waterInterval.toInt(),
                nurtureInterval.toInt(),
                PlantHelper.getWaterAmount(columns[5] as Long),
                PlantHelper.getPlacement(columns[6] as Long),
                DateHelper.dateFromDateString(columns[7] as String),
                DateHelper.dateFromDateString(columns[8] as String))
    }

}

class IntervalParser: RowParser<Pair<Int, Date>> {
    override fun parseRow(columns: Array<Any?>): Pair<Int, Date>{
        val idLong = columns[0] as Long
        val dateString = columns[1] as String
        return Pair(idLong.toInt(), DateHelper.dateFromDateString(dateString))
    }
}
