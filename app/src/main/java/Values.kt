/**
 * Created by wilhelm on 2017-10-25.
 */
object Values {
    val PLANT_INTENT_STRING = "plantIntent"
    val INTENT_UPDATE_LIST = 5
    val INTENT_PLANT_ID = "plant_id"
    val INTENT_PLANT_UPDATED = 200
    val INTENT_PLANT_CREATED = 201
    val INTENT_PLANT_DELETED = 400
    val FAILED_PLANT_ID = (-1)
    val ALARM_REQUEST_CODE = 0
    val PHOTO_REQUEST_CODE = 1
    val MY_REQUEST_CAMERA = 10
    val MY_REQUEST_WRITE_CAMERA = 11
    val NOTIFICATION_SETTINGS = "notificationSettings"
    val NOTIFICATION_HOUR = "notificationHour"
    val NOTIFICATION_MINUTE = "notificationMinute"
    val CHANNEL_NOTIF_ID = "vattna_reminder"
    val FIRST_LOG_IN = "first_login"
    var COMPLETED_ONBOARDING_PREF_NAME = "user_introduction"
}