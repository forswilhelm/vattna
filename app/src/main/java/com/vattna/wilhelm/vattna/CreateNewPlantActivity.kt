package com.vattna.wilhelm.vattna

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_create_new_plant.*
import watering
import placement
import Plant
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Environment
import android.provider.MediaStore
import android.renderscript.Sampler
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.squareup.picasso.MemoryPolicy
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import com.squareup.picasso.Picasso
import es.dmoral.toasty.Toasty
import helpers.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync

class CreateNewPlantActivity : AppCompatActivity() {

    private var userGeneratedImage = false
    private var mCurrentPhotoPath = ""
    private var UIResponsive = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_plant)
        val saveButton = findViewById<Button>(R.id.create_plant_save_button)
        saveButton.setOnClickListener { saveNewPlant() }

        setUpUI()
        setUpSwitch()
    }

    private fun setUpUI() {
        val waterArray = listOf<String>(getString(R.string.amply), getString(R.string.moderate), getString(R.string.sparingly))
        val placementArray = listOf<String>(getString(R.string.light), getString(R.string.semi_light), getString(R.string.dark))
        wateringSpinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, waterArray)
        placementSpinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, placementArray)
        plantView.setOnClickListener {
            if(PermissionHelper.checkPermissions(this)) {
                openCamera()
            }
        }
        plantName.setOnClickListener {
            val string = ""
            plantName.setText(string)
        }
        makeUIResponsive()
    }

    private fun setUpSwitch(){
        val checkbox = findViewById<CheckBox>(R.id.disable_nurture)
        checkbox.setOnCheckedChangeListener { _, isChecked ->
            val visib: Int
            if(isChecked) {
                visib = View.GONE
            } else {
                visib = View.VISIBLE
            }
            nurtureIntervalText.visibility = visib
            nurtureInterval.visibility = visib
            day.visibility = visib
        }
    }

    private fun saveNewPlant() {
        val wateringString = wateringSpinner.selectedItem as String
        val watering = PlantHelper.getWaterEnum(wateringString)
        val placementString = placementSpinner.selectedItem as String
        val placement = PlantHelper.getPlacementEnum(placementString)
        val name = plantName.text.toString()
        val wateringInterval = PlantHelper.getInt(wateringIntervalText.text)
        var nurtureInterval = PlantHelper.getInt(nurtureIntervalText.text)
        if (disable_nurture.isChecked) {
            nurtureInterval = 0
        }

        if (PlantHelper.isPlantValid(name, wateringInterval)) {
            makeUINonResponsive(getString(R.string.saving_flowers))
            val newPlant: Plant
            if (userGeneratedImage) {
                newPlant = Plant(name, mCurrentPhotoPath, wateringInterval, nurtureInterval, watering, placement)
            } else {
                newPlant = Plant(name, null, wateringInterval, nurtureInterval, watering, placement)
            }
            val id = DBHelper.getInstance(this).savePlant(newPlant)
            makeUIResponsive()
            Toasty.success(this, getString(R.string.plant_added),Toast.LENGTH_SHORT).show()
            setResult(Values.INTENT_PLANT_CREATED, Intent().putExtra(Values.INTENT_PLANT_ID, id))
            finish()
        } else {
            Toasty.error(this, getString(R.string.must_enter_all_fields), Toast.LENGTH_SHORT).show()
        }
    }


    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            val photoFile: File
            try {
                photoFile = createImageFile()
                val photoUri = FileProvider.getUriForFile(this, getString(R.string.fileProvider), photoFile)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            } catch(ex: IOException) {
                Log.d("CameraERROR", ex.message)
            }
            startActivityForResult(intent, Values.PHOTO_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val plantView = findViewById<CircleImageView>(R.id.plantView)
        if(resultCode == 0) {
            userGeneratedImage = false
        } else {
            //val image = imageHelper.getCorrectRotation(mCurrentPhotoPath, plantView.height, plantView.width)
            //Picasso.with(this).load(imageHelper.getImageUri(this, image)).fit().centerCrop().into(plantView)
            Picasso.with(this).load(File(mCurrentPhotoPath)).memoryPolicy(MemoryPolicy.NO_CACHE).fit().centerCrop().into(plantView)
            userGeneratedImage = true
            //image.recycle()

            alert(getString(R.string.use_plant_identification), getString(R.string.identify)) { positiveButton(getString(R.string.YES), {
                if(isNetWorkAvailable()) {
                    sendImageToVisionAPI()
                } else {
                    alert(getString(R.string.not_connected_to_internet)) {
                        positiveButton("OK"){}
                    }.show()
                }

            })
            negativeButton(getString(R.string.NO), {})
            }.show()
        }
    }

    private fun createDialog(array: Array<String>) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.what_plant_is_on_picture))
        .setItems(array, DialogInterface.OnClickListener { _, position ->
            if((position+1) != array.size) {
                plantName.setText(array[position])
            }
        })
        val alert = builder.create()
        makeUIResponsive()
        alert.show()

    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale("sv")).format(Date())
        val imageFileName = "JPEG_"+timeStamp+"_"
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    private fun sendImageToVisionAPI() {
        makeUINonResponsive(getString(R.string.looking_for_flowers))
        doAsync {
            val list = VisionHelper.getLabelDetection(ImageHelper.getCorrectRotation(mCurrentPhotoPath, ImageHelper.getOptionsLarge()), applicationContext)
            val array = list.toTypedArray()

            runOnUiThread(Runnable {
                if(array.size > 1) {
                    createDialog(array)
                } else {
                    noResultArray()
                }

            })
        }
    }


    private fun makeUINonResponsive(reason: String) {
        create_plant_shade.visibility = View.VISIBLE
        loading_text.visibility = View.VISIBLE
        loading_text.text = reason
        create_plant_save_button.visibility = View.GONE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        UIResponsive = false
    }

    private fun makeUIResponsive() {
        create_plant_shade.visibility = View.GONE
        loading_text.visibility = View.GONE
        create_plant_save_button.visibility = View.VISIBLE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        UIResponsive = true
    }

    private fun isNetWorkAvailable(): Boolean{
        val connectivity = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivity.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    override fun onBackPressed() {
        if(UIResponsive) {
            super.onBackPressed()
            finish()
        }
    }
    private fun noResultArray() {
        alert(getString(R.string.no_results_found)) {
            positiveButton("OK",{})
        }.show()
        makeUIResponsive()
    }
}
