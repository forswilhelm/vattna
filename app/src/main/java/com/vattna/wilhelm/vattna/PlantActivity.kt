package com.vattna.wilhelm.vattna

import helpers.DBHelper
import helpers.DateHelper
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import Plant
import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import es.dmoral.toasty.Toasty
import helpers.PlantHelper
import kotlinx.android.synthetic.main.activity_plant.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import java.io.File

class PlantActivity : AppCompatActivity() {

    lateinit var plant: Plant
    private var newlyCreated = true
    private var isChanged = false
    private val db = DBHelper.getInstance(this)
    private lateinit var deleteIcon: Drawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plant)
        val toolBar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolBar)
        val settingsIcon = ContextCompat.getDrawable(this, R.drawable.ic_action_settings_white)
        toolBar.overflowIcon = settingsIcon

        val plantId = intent.getIntExtra(Values.PLANT_INTENT_STRING, Values.FAILED_PLANT_ID)
        if(plantId != Values.FAILED_PLANT_ID) {
            plant = db.getPlant(plantId)
            updateUIWithPlant(plant)
        } else {
            Toast.makeText(this, getString(R.string.could_not_load_plant), Toast.LENGTH_SHORT).show()
            finish()
        }

        setUpButtons()
        newlyCreated = true
        val icon = ResourcesCompat.getDrawable(resources, R.drawable.ic_action_delete, null)
        if(icon != null) {
            deleteIcon = icon
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.plant_menu_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.plant_settings_edit -> editPlant()
            R.id.plant_settings_delete_watering -> deleteWatering()
            R.id.plant_settings_delete_nurture -> deleteNurture()
            R.id.plant_settings_delete_plant -> deletePlant()
            else -> return super.onOptionsItemSelected(item)
        }

        return true
    }

    override fun onResume() {
        super.onResume()
        if(!newlyCreated) {
            plant = db.getPlant(plant.id)
            updateUIWithPlant(plant)
        }
        newlyCreated = false
    }

    private fun waterPlant() {
        val message: String
        if(plant.daysToWater() > 1) {
            message = getString(R.string.are_you_sure_give_water)+" ${plant.daysToWater()} "+getString(R.string.days_until_watering)
        } else {
            message = getString(R.string.do_you_want_to_water)
        }
        alert(message, getString(R.string.do_watering)) {
            positiveButton(getString(R.string.do_watering)) {
                plant.water()
                doAsync { db.water(plant.id, plant.latestWatering) }
                watered_card_value.text = DateHelper.simpleDateString(plant.latestWatering)
                water_card_value.text = plant.daysToWater().toString()
                Toasty.success(applicationContext, getString(R.string.plant_watered)).show()
                isChanged = true
            }
            negativeButton(getString(R.string.cancel)) {

            }
        }.show()
    }

    private fun nurturePlant() {
        val message: String
        if(plant.daysToNurture() > 1) {
            message = getString(R.string.are_you_sure_give_nurture)+" ${plant.daysToNurture()} "+ getString(R.string.days_until_nurture)
        } else {
            message = getString(R.string.want_to_give_nurture)
        }
        alert(message, getString(R.string.give_nurture)) {
            positiveButton(getString(R.string.give_nurture)) {
                plant.nurture()
                doAsync { db.nurture(plant.id, plant.latestNurture) }
                nurtured_card_value.text = DateHelper.simpleDateString(plant.latestNurture)
                nurture_card_value.text = plant.daysToNurture().toString()
                Toasty.success(applicationContext, getString(R.string.plant_has_been_nurtured)).show()
            }
            negativeButton(R.string.cancel) {

            }
        }.show()
    }

    private fun updateUIWithPlant(plant: Plant) {
        val plantNameLabel = findViewById<TextView>(R.id.plant_name)
        val watered = findViewById<TextView>(R.id.watered_card_value)
        val water = findViewById<TextView>(R.id.water_card_value)
        val waterText = findViewById<TextView>(R.id.watering_amount_label)
        val placementText = findViewById<TextView>(R.id.placement_text)
        val waterIntervalText = findViewById<TextView>(R.id.plant_view_watering_interval)

        setImage()
        setNurtureTexts(plant)

        plantNameLabel.text = plant.name
        watered.text = DateHelper.simpleDateString(plant.latestWatering)
        water.text = plant.daysToWater().toString()



        val watertext = getString(R.string.gets_watered) +" "+ PlantHelper.getWaterString(plant.watering, this)
        val placementtext = getString(R.string.gets_placed) +" "+ PlantHelper.getPlacementString(plant.placement, this)
        val wateringInterval = getString(R.string.water_every) + " ${plant.waterInterval}" + getString(R.string.day)
        waterText.text = watertext
        placementText.text = placementtext
        waterIntervalText.text = wateringInterval
    }

    private fun setNurtureTexts(plant: Plant) {
        val nurtured = findViewById<TextView>(R.id.nurtured_card_value)
        val nurture = findViewById<TextView>(R.id.nurture_card_value)
        val nurtureIntervalText = findViewById<TextView>(R.id.plant_view_nurture_interval)
        if(plant.nurtureInterval > 0) {
            val nurtureInterval = getString(R.string.nurture_every) + " ${plant.nurtureInterval}" + getString(R.string.day)
            nurtured.text = DateHelper.simpleDateString(plant.latestNurture)
            nurture.text = plant.daysToNurture().toString()
            nurtureIntervalText.text = nurtureInterval
        } else {
            nurture.text = "-"
            nurtured.text = "-"
            nurtureIntervalText.visibility = View.GONE
            nurture_button.visibility = View.GONE
        }
    }

    private fun setUpButtons() {
        water_button.setOnClickListener { waterPlant() }
        nurture_button.setOnClickListener { nurturePlant() }
    }
    private fun editPlant() {
        val intent = Intent(this, EditPlantActivity::class.java)
        intent.putExtra(Values.PLANT_INTENT_STRING, plant.id)
        isChanged = true
        startActivity(intent)
    }
    private fun deleteNurture() {
        alert(getString(R.string.are_you_sure_delete_nurture), getString(R.string.delete_nurture)) {
            positiveButton(getString(R.string.undo)) {
                plant.latestNurture = db.deleteLatestNurture(plant)
                updateUIWithPlant(plant)
                Toasty.normal(applicationContext, getString(R.string.nurture_deleted), Toast.LENGTH_SHORT, deleteIcon).show()
            }
            negativeButton(getString(R.string.cancel)) {
            }
        }.show()
    }
    private fun deleteWatering() {
        alert(getString(R.string.are_you_sure_delete_watering), getString(R.string.delete_watering)) {
            positiveButton(getString(R.string.undo)) {
                plant.latestWatering = db.deleteLatestWatering(plant)
                updateUIWithPlant(plant)
                Toasty.normal(applicationContext, getString(R.string.watering_deleted), Toast.LENGTH_SHORT, deleteIcon/*getDrawable(R.drawable.ic_action_delete)*/).show()
                isChanged = true
            }
            negativeButton(getString(R.string.cancel)) {
            }
        }.show()
    }
    private fun deletePlant() {
        alert(getString(R.string.are_you_sure_delete_plant), getString(R.string.delete)) {
            positiveButton(getString(R.string.delete)) {
                db.deletePlant(plant.id)
                Toasty.normal(applicationContext, getString(R.string.plant_deleted), Toast.LENGTH_SHORT, deleteIcon).show()
                setResult(Values.INTENT_PLANT_DELETED, Intent().putExtra(Values.INTENT_PLANT_ID, plant.id))
                finish()
            }
            negativeButton(getString(R.string.cancel)) {
            }
        }.show()
    }

    private fun setImage() {
        val image = plant.imagePath
        if(image != null) {
            Picasso.with(this).load(File(image)).memoryPolicy(MemoryPolicy.NO_CACHE).fit().centerCrop().into(plant_view_image)
        } else {
            Picasso.with(this).load(R.drawable.plants).fit().centerCrop().into(plant_view_image)
        }
    }

    override fun onBackPressed() {
        if(isChanged) {
            setResult(Values.INTENT_PLANT_UPDATED, Intent().putExtra(Values.INTENT_PLANT_ID, plant.id))
        }
        super.onBackPressed()
        finish()
    }

}
