package com.vattna.wilhelm.vattna

import android.Manifest
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment


class IntroActivity : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bgColor = Color.rgb(7, 123, 38)

        addSlide(AppIntroFragment.newInstance(getString(R.string.welcome_to_vattna), null, getString(R.string.description_long), null,  R.drawable.vattna_logo_light, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.add_plant),null, getString(R.string.how_to_create_pt1),null, R.drawable.how_to_add_small, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.add_plant_p2), null,getString(R.string.how_to_create_p2), null, R.drawable.how_to_create_small, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.take_picture),null, getString(R.string.how_to_identify), null, R.drawable.how_to_photo_small, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.use_identification), null,  getString(R.string.how_to_identify_p2), null, R.drawable.how_to_identify_small, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.overview), null, getString(R.string.how_to_list_view), null, R.drawable.how_to_list_small, bgColor, Color.WHITE, Color.WHITE))
        addSlide(AppIntroFragment.newInstance(getString(R.string.plant_view), null,getString(R.string.how_to_plant_view), null, R.drawable.how_to_plant_view_small, bgColor, Color.WHITE, Color.WHITE))
        if(Build.VERSION.SDK_INT >= 24) {
            addSlide(AppIntroFragment.newInstance(getString(R.string.reminders), null, getString(R.string.how_to_notification), null, R.drawable.how_to_set_notif_small, bgColor, Color.WHITE, Color.WHITE))
        }

        askForPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 4)

        showSkipButton(false)
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        val sharedPreferences = getSharedPreferences(Values.FIRST_LOG_IN, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(Values.COMPLETED_ONBOARDING_PREF_NAME, true)
        editor.apply()
        finish()
    }
}
