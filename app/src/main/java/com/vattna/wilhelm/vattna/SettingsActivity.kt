package com.vattna.wilhelm.vattna


import android.app.TimePickerDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.*
import java.util.*
import android.util.Log
import es.dmoral.toasty.Toasty
import helpers.AlarmHelper


class SettingsActivity : AppCompatActivity(), TimePickerDialog.OnTimeSetListener {

    lateinit var saveButton: Button
    lateinit var timeButton: Button
    private var hour = 18
    private var minute = 0
    private var changed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        val toolBar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolBar)
        setUpButtons()
    }


    private fun setUpButtons() {
        saveButton = findViewById(R.id.save_button_settings)
        timeButton = findViewById(R.id.time_button)
        saveButton.setOnClickListener { save() }
        val sharedPreferences = this.getSharedPreferences(Values.NOTIFICATION_SETTINGS, Context.MODE_PRIVATE)
        hour = sharedPreferences.getInt(Values.NOTIFICATION_HOUR, 18)
        minute = sharedPreferences.getInt(Values.NOTIFICATION_MINUTE, 0)
        timeButton.text = timeString(hour, minute)
    }

    private fun save() {
        if(changed) {
            val sharedPreferences = getSharedPreferences(Values.NOTIFICATION_SETTINGS, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()

            editor.putInt(Values.NOTIFICATION_HOUR, hour)
            editor.putInt(Values.NOTIFICATION_MINUTE, minute)
            editor.apply()

            AlarmHelper(this).setAlarm()
        }

        //Toast.makeText(this, , Toast.LENGTH_SHORT).show()
        Toasty.success(this, getString(R.string.settings_saved)).show()
        finish()
    }

    fun setNotificationTime(view: View) {
        val dialog = TimePickerDialog(this, this, 0, 0, true)
        dialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        timeButton.text = timeString(hourOfDay, minute)
        hour = hourOfDay
        this.minute = minute
        changed = true
    }



    private fun timeString(hours: Int, minutes: Int): String {
        val newHour: String
        val newMinute: String
        if(hours >= 10) {
            newHour = hours.toString()
        } else {
            newHour = "0$hours"
        }
        if(minutes >= 10) {
            newMinute = minutes.toString()
        } else {
            newMinute = "0$minutes"
        }
        return newHour + ":" + newMinute
    }

}
