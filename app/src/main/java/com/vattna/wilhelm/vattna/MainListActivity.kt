package com.vattna.wilhelm.vattna

import helpers.DBHelper
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import Plant
import android.content.Intent
import android.view.*
import android.support.v7.widget.Toolbar
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main_list.*
import RecyclerPlantListAdapter
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import org.jetbrains.anko.doAsync
import android.os.Build
import helpers.AlarmHelper
import ClickListener
import android.util.DisplayMetrics
import android.util.Log
import java.util.*


class MainListActivity : AppCompatActivity(), ClickListener {

    lateinit private var adapter: RecyclerPlantListAdapter
    lateinit private var listOfPlants: List<Plant>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_list)
        val toolBar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolBar)

        setLanguage()

        showIntro()

        fab.setOnClickListener { _ ->
            val intent = Intent(this, CreateNewPlantActivity::class.java)
            startActivityForResult(intent, Values.INTENT_UPDATE_LIST)
        }
        setUpList()

        if (Build.VERSION.SDK_INT >= 24) {
            AlarmHelper(this).setAlarm()
        }

    }

    private fun setUpList() {
        listOfPlants = DBHelper.getInstance(this).getPlants()
        val plantList = findViewById<RecyclerView>(R.id.plant_list)
        plantList.setHasFixedSize(true)
        plantList.setItemViewCacheSize(20)
        plantList.isDrawingCacheEnabled = true
        plantList.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        plantList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapter = RecyclerPlantListAdapter(this, listOfPlants, this)
        plantList.adapter = adapter
    }

    private fun setLanguage() {
        val conf = resources.configuration
        if(conf.locale != Locale("sv", "SE")) {
            Log.d("LANG", "Setting language to english")
            conf.setLocale(Locale.UK)
            resources.updateConfiguration(conf, DisplayMetrics())
        }
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, PlantActivity::class.java)
        intent.putExtra(Values.PLANT_INTENT_STRING, adapter.plants[position].id)
        startActivityForResult(intent, Values.INTENT_UPDATE_LIST)
    }

    private fun showIntro() {
        val sharedPreferences = getSharedPreferences(Values.FIRST_LOG_IN, Context.MODE_PRIVATE)
        if (!sharedPreferences.getBoolean(
                Values.COMPLETED_ONBOARDING_PREF_NAME, false)) {
            startActivity(Intent(this, IntroActivity::class.java))
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if(Build.VERSION.SDK_INT >= 24) {
            menuInflater.inflate(R.menu.menu_main_list, menu)
            return true
        } else {
            menuInflater.inflate(R.menu.menu_main_under_sdk_24, menu)
            return true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings ->  {
                startActivity(Intent(this, SettingsActivity::class.java))
            }
            R.id.show_tutorial -> {
                startActivity(Intent(this, IntroActivity::class.java))
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val id = data.getIntExtra(Values.INTENT_PLANT_ID, 0)
            if(requestCode == Values.INTENT_UPDATE_LIST) {
                when (resultCode) {
                    Values.INTENT_PLANT_UPDATED -> updateListWithUpdatedPlant(id)
                    Values.INTENT_PLANT_CREATED -> updateListWithCreatedPlant(id)
                    Values.INTENT_PLANT_DELETED -> updateListWithDeletedPlant(id)
                }
            }

        }
    }

    private fun updateListWithDeletedPlant(id: Int) {
        val newList = adapter.plants.filter {
            it.id != id
        }
        val ids = arrayListOf<Int>()
        adapter.plants.forEach {
            ids.add(it.id)
        }
        adapter.plants = newList
        adapter.notifyItemRemoved(ids.indexOf(id))

    }

    private fun updateListWithCreatedPlant(id: Int) {
        doAsync {
            if(adapter.plants.isEmpty()) {
                val intent = Intent(applicationContext, MainListActivity::class.java)
                startActivity(intent)
                finish()
            }
            val plant = DBHelper.getInstance(applicationContext).getPlant(id)
            val newList = adapter.plants.plus(plant)
            adapter.plants = newList
            adapter.plants
            Log.d("Index is ", "${newList.indexOf(plant)}")
            adapter.notifyItemInserted(newList.indexOf(plant))
        }
    }

    private fun updateListWithUpdatedPlant(id: Int) {
        doAsync {
            val plant = DBHelper.getInstance(applicationContext).getPlant(id)
            val list = adapter.plants
            val updatedList = list.map {
                updatePlant(it, plant)
            }
            adapter.plants = updatedList
            adapter.notifyItemChanged(updatedList.indexOf(plant))
        }
    }

    private fun updatePlant(oldPlant: Plant, newPlant: Plant): Plant {
        if(oldPlant.id == newPlant.id) {
            return newPlant
        } else {
            return oldPlant
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }


}

