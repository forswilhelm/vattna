package com.vattna.wilhelm.vattna

import helpers.DBHelper
import helpers.PlantHelper
import helpers.ImageHelper
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import Plant
import android.content.Intent
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.util.Log
import watering
import placement
import android.widget.ArrayAdapter
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import helpers.PermissionHelper
import kotlinx.android.synthetic.main.activity_create_new_plant.*
import kotlinx.android.synthetic.main.activity_edit_plant.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class EditPlantActivity : AppCompatActivity() {

    lateinit var plant: Plant
    var mCurrentPhotoPath = ""
    var changePlantImage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_plant)


        val plantId = intent.getIntExtra(Values.PLANT_INTENT_STRING, Values.FAILED_PLANT_ID)
        if(plantId != Values.FAILED_PLANT_ID) {
            plant = DBHelper.getInstance(this).getPlant(plantId)
            setPlantInfo()
        } else {
            Toast.makeText(this, getString(R.string.could_not_load_plant), Toast.LENGTH_SHORT).show()
            finish()
        }
        setPlantInfo()
        edit_plant_save_button.setOnClickListener { updatePlant() }
        edit_plant_view.setOnClickListener { changePlantImage() }

    }
    fun setPlantInfo() {
        edit_watering_spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listOf<String>(getString(R.string.amply), getString(R.string.moderate), getString(R.string.sparingly)))
        edit_placement_spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, listOf<String>(getString(R.string.light), getString(R.string.semi_light), getString(R.string.dark)))
        edit_watering_spinner.setSelection(PlantHelper.getWaterAmountInt(plant.watering)-1)
        edit_placement_spinner.setSelection(PlantHelper.getPlacementInt(plant.placement)-1)
        edit_plant_name.setText(plant.name)
        edit_nurture_interval_text.setText(plant.nurtureInterval.toString())
        edit_watering_interval_text.setText(plant.waterInterval.toString())
        if(!plant.imagePath.isNullOrEmpty()) {
            val plantView = findViewById<CircleImageView>(R.id.edit_plant_view)
            //Picasso.with(this).load(imageHelper.getImageUri(this, imageHelper.getCorrectRotation(plant.imagePath!!, plantView.height, plantView.width))).fit().centerCrop().into(plantView)
            Picasso.with(this).load(File(plant.imagePath!!)).memoryPolicy(MemoryPolicy.NO_CACHE).fit().centerCrop().into(plantView)
        }

    }

    fun updatePlant() {
        val plantName = edit_plant_name.text.toString()
        val placementString = edit_placement_spinner.selectedItem as String
        val wateringString = edit_watering_spinner.selectedItem as String
        val watering = PlantHelper.getWaterEnum(wateringString)
        val placement = PlantHelper.getPlacementEnum(placementString)
        val nurtureInterval = PlantHelper.getInt(edit_nurture_interval_text.text)
        val waterInterval = PlantHelper.getInt(edit_watering_interval_text.text)
        val imagePath: String?
        if(changePlantImage) {
            imagePath = mCurrentPhotoPath
        } else {
            imagePath = plant.imagePath
        }

        val updatedPlant = Plant(plant.id,
                                plantName,
                                imagePath,
                                waterInterval,
                                nurtureInterval,
                                watering,
                                placement,
                                plant.latestWatering,
                                plant.latestNurture)

        DBHelper.getInstance(this).updatePlant(updatedPlant)

        finish()
    }

    private fun changePlantImage() {
        if(PermissionHelper.checkPermissions(this)) {
            openCamera()
        }
    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(packageManager) != null) {
            val photoFile: File
            try {
                photoFile = createImageFile()
                val photoUri = FileProvider.getUriForFile(this, getString(R.string.fileProvider), photoFile)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            } catch(ex: IOException) {
                Log.d("CameraERROR", ex.message)
            }
            startActivityForResult(intent, Values.PHOTO_REQUEST_CODE)
        }
    }

    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale("sv")).format(Date())
        val imageFileName = "JPEG_"+timeStamp+"_"
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("CAMERA CALLBACK: ", "$requestCode, $resultCode, $data")
        val plantView = findViewById<CircleImageView>(R.id.edit_plant_view)
        if(resultCode == 0) {
            changePlantImage = false
        } else {
            //val image = imageHelper.getCorrectRotation(mCurrentPhotoPath, plantView.height, plantView.width)
            //Picasso.with(this).load(imageHelper.getImageUri(this, image)).fit().centerCrop().into(plantView)
            Picasso.with(this).load(File(mCurrentPhotoPath)).fit().memoryPolicy(MemoryPolicy.NO_CACHE).centerCrop().into(plantView)
            changePlantImage = true
            //image.recycle()
        }
    }
}
