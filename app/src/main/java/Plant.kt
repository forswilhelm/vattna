
import helpers.DateHelper
import android.util.Log
import java.util.*

/**
 * Created by wilhelm on 2017-10-20.
 */
class Plant constructor (val id: Int,
                         var name: String,
                         val imagePath: String?,
                         var waterInterval: Int,
                         var nurtureInterval: Int,
                         var watering: Enum<watering>,
                         var placement: Enum<placement>,
                         var latestWatering: Date,
                         var latestNurture: Date) {

    fun water() {
        latestWatering = Date()
    }

    fun nurture() {
        latestNurture = Date()
    }

    fun daysToWater() : Int {
        return daysUntil(latestWatering, waterInterval)
    }

    fun daysToNurture() : Int {
        return daysUntil(latestNurture, nurtureInterval)
    }

    private fun daysUntil(date: Date, interval: Int): Int {
        val daysTo = interval - DateHelper.daysSinceDate(date)
        if(daysTo > 0) {
            return daysTo
        } else {
            return 0
        }


    }

    constructor(
            name: String,
            imagePath: String?,
            waterInterval: Int,
            nurtureInterval: Int,
            watering: Enum<watering>,
            placement: Enum<placement>
    ) : this(
            0,
            name,
            imagePath,
            waterInterval,
            nurtureInterval,
            watering,
            placement,
            Date(),
            Date()
    )
}