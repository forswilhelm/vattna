package helpers

import android.content.Context
import android.graphics.Bitmap
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.vision.v1.Vision
import com.google.api.services.vision.v1.VisionRequestInitializer
import com.google.api.services.vision.v1.model.*
import com.vattna.wilhelm.vattna.R
import java.io.ByteArrayOutputStream

/**
 * Created by wilhelm on 2017-11-09.
 */
class VisionHelper{

    companion object {
        private val api_key = "AIzaSyDfXWBJAcv-aIAoCUJlRE_e2WwHl60Mb7E"

        fun getLabelDetection(bitmap: Bitmap, context: Context): List<String> {
            val feature = Feature()
            feature.type = "LABEL_DETECTION"
            feature.maxResults = 7

            val request = AnnotateImageRequest()
            request.features = listOf(feature)
            request.image = getEncodedImage(bitmap)
            bitmap.recycle()
            val annotateImageRequests = listOf(request)

            val httpTransport = AndroidHttp.newCompatibleTransport()
            val jsonFactory = GsonFactory.getDefaultInstance()
            val visionRequestinitializer = VisionRequestInitializer(api_key)
            val builder = Vision.Builder(httpTransport, jsonFactory, null).setApplicationName(context.getString(R.string.app_name))

            builder.setVisionRequestInitializer(visionRequestinitializer)

            val vision = builder.build()

            val batchAnnotateImagesRequest = BatchAnnotateImagesRequest()
            batchAnnotateImagesRequest.requests = annotateImageRequests

            val annotateRequest = vision.images().annotate(batchAnnotateImagesRequest)
            annotateRequest.disableGZipContent = true
            val response = annotateRequest.execute()
            val responses = getStringArray(response, context)
            return responses.filter {
                filterResponses(it)

            }.map {
                removeFamily(it)
            }
        }

        private fun getEncodedImage(bitmap: Bitmap): Image {
            val encodedImage = Image()
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos)
            val imageBytes = baos.toByteArray()
            encodedImage.encodeContent(imageBytes)
            bitmap.recycle()
            return encodedImage
        }

        private fun getStringArray(response: BatchAnnotateImagesResponse, context: Context): List<String> {
            val list: MutableList<String> = mutableListOf()
            val imageResponses = response.responses[0]

            val entityAnnotations = imageResponses.labelAnnotations
            entityAnnotations.forEach {
                list.add(it.description.capitalize())
            }
            list.add(context.getString(R.string.none_of_the_above))
            return list
        }

        private fun filterResponses(name: String): Boolean {

            nonFlowerNames.forEach {
                if (name == it) {
                    return false
                }
            }
            return true
        }

        private fun removeFamily(name: String): String {
            if(name.toLowerCase().contains(" family")) {
                name.replace(" family","")
            }
            return name
        }

        private val nonFlowerNames = arrayOf("Plant", "Flower", "Flowering plant", "Flora",
                "Green", "Flowerpot", "Spring", "Summer", "Autumn",
                "Sun", "Water", "Pot", "Red", "Yellow", "Violet","Purple",
                "Blue", "White", "Garden", "Petals", "Flower Petals",
                "Petal", "Root", "Stem", "Flower pot", "Pink",
                "Nature", "Leaf", "Plantstem", "Houseplant",
                "Seed plant", "Shrub", "Plant stem", "Herb", "Tree", "Wildflower",
                "Grass")


    }


}