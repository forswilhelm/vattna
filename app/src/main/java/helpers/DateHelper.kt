package helpers

import android.content.Context
import com.vattna.wilhelm.vattna.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by wilhelm on 2017-10-25.
 */
class DateHelper {

    companion object {
        private val regularDateFormat = SimpleDateFormat("yyyy.MM.dd", Locale("sv"))
        fun simpleDateString(date: Date): String {
            val calendar = Calendar.getInstance()
            calendar.time = date
            val month = calendar.get(Calendar.MONTH) + 1
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val dateString = "$day/$month"
            return dateString
        }

        fun dateStringFromDate(date: Date): String {
            val dateString = regularDateFormat.format(date)
            return dateString
        }

        fun dateFromDateString(dateString: String): Date {
            return regularDateFormat.parse(dateString)
        }

        fun daysSinceDate(date: Date): Int {
            val diff = Date().time - date.time
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
        }

        fun dayDateString(inDays: Int, context: Context): String {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, inDays)
            return dayDateString(calendar.time, context)
        }

        fun dayDateString(date: Date, context: Context): String {
            val todayCalendar = Calendar.getInstance()
            val dateCalendar = Calendar.getInstance()
            dateCalendar.time = date
            if(regularDateFormat.format(todayCalendar.time) == regularDateFormat.format(dateCalendar.time)) {
                return context.getString(R.string.today)
            }
            val day = dayOfWeek(dateCalendar, context)
            val month = dateCalendar.get(Calendar.MONTH) + 1
            val date2 = dateCalendar.get(Calendar.DAY_OF_MONTH)

            return "$day $date2/$month"


        }

        fun dayOfWeek(calendar: Calendar, context: Context): String {
            val day: String
            when(calendar.get(Calendar.DAY_OF_WEEK)) {
                Calendar.MONDAY -> day = context.getString(R.string.monday_short)
                Calendar.TUESDAY -> day = context.getString(R.string.tuesday_short)
                Calendar.WEDNESDAY -> day = context.getString(R.string.wednesday_short)
                Calendar.THURSDAY -> day = context.getString(R.string.thursday_short)
                Calendar.FRIDAY -> day = context.getString(R.string.friday_short)
                Calendar.SATURDAY -> day = context.getString(R.string.saturday_short)
                Calendar.SUNDAY -> day = context.getString(R.string.sunday_short)
                else -> day = ""
            }

            return day
        }
    }
}