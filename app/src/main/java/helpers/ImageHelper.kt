package helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.R.drawable
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import android.support.v4.R
import android.util.Log
import com.google.api.services.vision.v1.model.Image
import java.io.ByteArrayOutputStream

/**
 * Created by wilhelm on 2017-10-31.
 */
class ImageHelper {

    companion object {
        fun getOptionsSmall(): BitmapFactory.Options {
            val options = BitmapFactory.Options()
            options.inSampleSize = 8
            options.inPreferredConfig = Bitmap.Config.RGB_565
            return options
        }

        fun getOptionsLarge(): BitmapFactory.Options {
            val options = BitmapFactory.Options()
            options.inSampleSize = 2
            options.inPreferredConfig = Bitmap.Config.RGB_565
            return options
        }

        fun getImageUri(context: Context, bitmap: Bitmap): Uri {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
            return Uri.parse(path)
        }

        fun getCorrectRotation(imagePath: String, options: BitmapFactory.Options): Bitmap {
            val ei = ExifInterface(imagePath)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED)
            val imageBitmap = BitmapFactory.decodeFile(imagePath, options)
            val rotatedBitmap: Bitmap

            when(orientation) {

                ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(imageBitmap, 90.toFloat())
                ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(imageBitmap, 180.toFloat())
                ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(imageBitmap, 270.toFloat())
                else -> rotatedBitmap = imageBitmap
            }
            return rotatedBitmap
        }

        private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            val bitmap = Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
            return bitmap
        }

        fun getCorrectRotation(imagePath: String, width: Int, height: Int): Bitmap {
            val ei = ExifInterface(imagePath)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED)
            val options = BitmapFactory.Options()
            options.inSampleSize = 2
            //options.outHeight= height
            //options.outWidth = width
            val imageBitmap = BitmapFactory.decodeFile(imagePath, options)
            val rotatedBitmap: Bitmap

            when(orientation) {

                ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(imageBitmap, 90.toFloat())
                ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(imageBitmap, 180.toFloat())
                ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(imageBitmap, 270.toFloat())
                else -> rotatedBitmap = imageBitmap
            }
            return rotatedBitmap
        }
        private fun calculateSampleSize(options: BitmapFactory.Options ,reqHeight: Int, reqWidth: Int): Int {
            val height = options.outHeight
            val width = options.outWidth
            var inSampleSize = 1
            Log.d("Calculating", "Height: $height, Width: $width")

            if(height > reqHeight || width > reqWidth) {
                val halfHeight = height / 2
                val halfWidth = width / 2
                Log.d("Calculating", "halfHeight: $halfHeight, halfWidth: $halfWidth, inSampleSize: $inSampleSize")
                while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                    inSampleSize *= 2
                }
            }
            Log.d("ImageHelper", "inSampleSize set to $inSampleSize")
            return inSampleSize
        }
    }

}