package helpers

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.vattna.wilhelm.vattna.MainListActivity
import com.vattna.wilhelm.vattna.R
import java.util.*

/**
 * Created by wilhelm on 2017-11-15.
 */
class AlarmHelper(val context: Context) {

    fun setAlarm() {
        if (Build.VERSION.SDK_INT >= 24) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.set(AlarmManager.RTC_WAKEUP, getAlarmTime(), "ALARM_TAG", AlarmManager.OnAlarmListener { sendNotification() }, Handler())
            Log.d("Setting alarm", "Alarm set")
        }
    }

    private fun sendNotification() {

        val db = DBHelper(context)
        if(db.timeToWater()) {
            Log.d("--Alarm--", "Alarm went off, flowers to water")
            if(Build.VERSION.SDK_INT >= 26) {
                sendAdvancedNotification()
            } else  {
                sendRegularNotification()
            }
        } else {
            Log.d("--Alarm--", "Alarm went off, NO flowers to water")
        }
        setAlarm()
    }

    private fun sendAdvancedNotification() {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            val notifMan = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val id = Values.CHANNEL_NOTIF_ID
            val name = context.getString(R.string.reminders_to_water)
            val description = context.getString(R.string.here_are_water_reminders)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(id, name, importance)
            channel.description = description
            channel.enableLights(true)
            channel.lightColor = Color.GREEN
            channel.enableVibration(true)
            val array = kotlin.LongArray(4)
            array[0] = 100
            array[1] = 100
            array[2] = 100
            array[3] = 100
            channel.vibrationPattern = array

            notifMan.createNotificationChannel(channel)

            val builder = NotificationCompat.Builder(context, channel.id)
                    .setSmallIcon(R.drawable.flower_logo_green)
                    .setContentTitle(context.getString(R.string.time_to_water))
                    .setContentText(context.getString(R.string.notification_message))
            val resultIntent = Intent(context, MainListActivity::class.java)
            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addParentStack(MainListActivity::class.java)
            stackBuilder.addNextIntent(resultIntent)
            val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            builder.setContentIntent(pendingIntent)
            notifMan.notify(100, builder.build())

        }

    }

    private fun sendRegularNotification() {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val repeatingIntent = Intent(context, MainListActivity::class.java)
        repeatingIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        val pendingIntent = PendingIntent.getActivity(context, Values.ALARM_REQUEST_CODE, repeatingIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(context, Values.CHANNEL_NOTIF_ID)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.flower_logo_green)
                .setContentTitle(context.getString(R.string.time_to_water))
                .setContentText(context.getString(R.string.notification_message))
                .setAutoCancel(true)

        notificationManager.notify(Values.ALARM_REQUEST_CODE, builder.build())
    }

    private fun getAlarmTime():Long {
        val calendar = Calendar.getInstance()
        val sharedPreferences = context.getSharedPreferences(Values.NOTIFICATION_SETTINGS, Context.MODE_PRIVATE)
        val hour = sharedPreferences.getInt(Values.NOTIFICATION_HOUR, 18)
        val minute = sharedPreferences.getInt(Values.NOTIFICATION_MINUTE, 0)

        val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = calendar.get(Calendar.MINUTE)

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hour, minute)

        if((currentHour * 60) + currentMinute >= (hour * 60) + minute) {
            calendar.add(Calendar.DATE, 1)
        }
        return calendar.timeInMillis
    }
}