package helpers

import android.content.Context
import android.text.Editable
import com.vattna.wilhelm.vattna.R
import watering
import placement

/**
 * Created by wilhelm on 2017-10-26.
 */
class PlantHelper {
    companion object {
        fun getInt(editable: Editable): Int {
            if (editable.isNullOrEmpty()) {
                return 0
            } else {
                return editable.toString().toInt()
            }
        }

        fun getWaterAmountInt(waterAmount: Enum<watering>): Int {
            when(waterAmount) {
                watering.Sparsamt -> return 3
                watering.Måttligt -> return 2
                watering.Rikligt -> return 1
                else -> return 0
            }
        }

        fun getPlacementInt(placing: Enum<placement>): Int {
            when(placing) {
                placement.Mörkt -> return 3
                placement.Halvljust -> return 2
                placement.Ljust -> return 1
                else -> return 0
            }
        }

        fun getWaterAmount(ordinal: Long): watering {
            when(ordinal.toInt()) {
                3 -> return watering.Sparsamt
                2 -> return watering.Måttligt
                1 -> return watering.Rikligt
                else -> return watering.Måttligt
            }
        }

        fun getPlacement(ordinal: Long): placement {
            when(ordinal.toInt()) {
                3 -> return placement.Mörkt
                2 -> return placement.Halvljust
                1 -> return placement.Ljust
                else -> return placement.Halvljust
            }
        }

        fun isPlantValid(name: String, watering: Int): Boolean {
            if(name != "" && watering != 0) {
                return true
            }
            return false
        }

        fun getWaterEnum(amount: String): Enum<watering> {
            when(amount) {
                "Rikligt", "Amply" -> return watering.Rikligt
                "Måttligt", "Moderate" -> return watering.Måttligt
                "Sparsamt", "Sparingly" -> return watering.Sparsamt
                else -> return watering.Rikligt
            }
        }

        fun getPlacementEnum(place: String): Enum<placement> {
            when(place) {
                "Ljust", "Light" -> return placement.Ljust
                "Halvskugga", "Partial Shade" -> return placement.Halvljust
                "Skugga", "Shade" -> return placement.Mörkt
                else -> return placement.Ljust
            }
        }

        fun getWaterString(waterAmount: Enum<watering>, context: Context): String {
            when(waterAmount) {
                watering.Sparsamt -> return context.getString(R.string.sparingly)
                watering.Måttligt -> return context.getString(R.string.moderate)
                watering.Rikligt -> return context.getString(R.string.amply)
                else -> return context.getString(R.string.amply)
            }
        }

        fun getPlacementString(placing: Enum<placement>, context: Context): String {
            when(placing) {
                placement.Mörkt -> return context.getString(R.string.dark)
                placement.Halvljust -> return context.getString(R.string.semi_light)
                placement.Ljust -> return context.getString(R.string.light)
                else -> return context.getString(R.string.light)
            }
        }
    }

}