package helpers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log

/**
 * Created by wilhelm on 2017-11-07.
 */
class PermissionHelper {
    companion object {
        private val TAG = "--Permission Check--"
        fun checkPermissions(context: Context): Boolean {
            Log.d(TAG, "Entered permission check")
            val all = checkPermissionsFor(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), Values.MY_REQUEST_CAMERA, context)

            //val storage = checkPermissionsFor(Manifest.permission.WRITE_EXTERNAL_STORAGE, Values.MY_REQUEST_WRITE_CAMERA, context)
            //val camera = checkPermissionsFor(Manifest.permission.CAMERA, Values.MY_REQUEST_CAMERA, context)
            //return (storage && camera)
            Log.d(TAG, "Returning $all")
            return all
        }

        private fun checkPermissionsFor(permissionArray: Array<String>, permissionCode: Int, context: Context): Boolean {

            Log.d(TAG, "Checking permission for ${permissionArray.asList()}")
            val permissionCheck = hasPermission(context, permissionArray)
            if(!permissionCheck) {
                ActivityCompat.requestPermissions(context as Activity, permissionArray, permissionCode)
                return hasPermission(context, permissionArray)
            } else {
                Log.d("Permissions", "Permissions granted for ${permissionArray.asList()}")
                return true
            }
        }

        private fun hasPermission(context: Context, permissionArray: Array<String>): Boolean {
            for(permission in permissionArray) {
                if(ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
            return true
        }
    }

}