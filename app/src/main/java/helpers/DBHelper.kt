package helpers

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import org.jetbrains.anko.db.*
import java.util.*
import Plant
import PlantParser
import IntervalParser

/**
 * Created by wilhelm on 2017-10-26.
 */
class DBHelper(context: Context): ManagedSQLiteOpenHelper(context, "VattnaDatabase", null, 1) {


    companion object {
        private var instance: DBHelper? = null
        fun getInstance(ctx: Context): DBHelper {
            if (instance == null) {
                Log.d("DB", "Instance created")
                instance = DBHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        Log.d(DB_TAG, "ON CREATE")
        db.createTable(PLANT_TABLE,  true,
                PLANT_ID to INTEGER + PRIMARY_KEY + UNIQUE,
                PLANT_NAME to TEXT,
                PLANT_PHOTO to TEXT,
                PLANT_WATERINGINTERVAL to INTEGER,
                PLANT_NURTUREINTERVAL to INTEGER,
                PLANT_WATERING to INTEGER,
                PLANT_PLACEMENT to INTEGER,
                PLANT_LATESTWATERING to TEXT,
                PLANT_LATESTNURTURE to TEXT
                )
        Log.d(DB_TAG, "CREATED $PLANT_TABLE")
        db.createTable(WATERING_TABLE, true, WATERING_ID to INTEGER,
                WATERING_DATE to TEXT)
        db.createTable(NURTURE_TABLE, true, NURTURE_ID to INTEGER,
                NURTURE_DATE to TEXT)
        Log.d(DB_TAG, "CREATED $WATERING_TABLE and $NURTURE_TABLE")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable("User", true)
    }

    fun deleteDatabase() {
        Log.d(DB_TAG, "Database delete")
        writableDatabase.dropTable(PLANT_TABLE, true)
        writableDatabase.dropTable(WATERING_TABLE, true)
        writableDatabase.dropTable(NURTURE_TABLE, true)
        onCreate(writableDatabase)
    }

     //Access property for Context
    val Context.database: DBHelper
        get() = getInstance(applicationContext)

    fun savePlant(plant: Plant): Int {
        Log.d(DB_TAG, "Saving plant: ${plant.name}")

        val id: Long = writableDatabase.insert(PLANT_TABLE,
                PLANT_NAME to plant.name,
                PLANT_PHOTO to plant.imagePath,
                PLANT_WATERINGINTERVAL to plant.waterInterval,
                PLANT_NURTUREINTERVAL to plant.nurtureInterval,
                PLANT_WATERING to PlantHelper.getWaterAmountInt(plant.watering),
                PLANT_PLACEMENT to PlantHelper.getPlacementInt(plant.placement),
                PLANT_LATESTWATERING to DateHelper.dateStringFromDate(plant.latestWatering),
                PLANT_LATESTNURTURE to DateHelper.dateStringFromDate(plant.latestNurture)
                )

        water(id.toInt(), plant.latestWatering)
        nurture(id.toInt(), plant.latestNurture)

        Log.d("DB", "Plant created with id: $id")

        return id.toInt()
    }

    //PLANT
    fun getPlant(id: Int): Plant {
        val plant = readableDatabase.select(PLANT_TABLE).whereArgs(PLANT_ID+" = $id").parseSingle(PlantParser())
        Log.d(DB_TAG, "Getting single plant: ${plant.name}")
        return plant
    }

    fun getPlants(): List<Plant> {
        Log.d(DB_TAG, "Getting all plants")
        return  readableDatabase.select(PLANT_TABLE).parseList(PlantParser())
    }

    fun timeToWater(): Boolean {
        val list = readableDatabase.select(PLANT_TABLE).parseList(PlantParser())
        var timeToWater = false
        list.forEach {
            if(it.daysToWater() == 0) {
                timeToWater = true
            }
        }
        return timeToWater
    }

    fun updateLatestWatering(latestWatering: Date,id: Int) {
        Log.d(DB_TAG, "Updating last watering")
        val dateString = DateHelper.dateStringFromDate(latestWatering)
        writableDatabase.update(PLANT_TABLE, PLANT_LATESTWATERING to dateString).whereArgs(PLANT_ID+" = $id").exec()
    }
    fun updateLatestNurture(latestNurture: Date,id: Int) {
        Log.d(DB_TAG, "Updating last nurture")
        val dateString = DateHelper.dateStringFromDate(latestNurture)
        writableDatabase.update(PLANT_TABLE, PLANT_LATESTNURTURE to dateString).whereArgs(PLANT_ID+" = $id").exec()
    }
    fun updatePlant(plant: Plant) {
        Log.d(DB_TAG, "Updating plant: ${plant.name}")
        val plantValues = getValuesFromPlant(plant)
        writableDatabase.update(PLANT_TABLE, plantValues, "$PLANT_ID = ${plant.id}", null)
        writableDatabase.close()
    }
    fun deletePlant(id: Int) {
        Log.d(DB_TAG, "Deleting plant with id: $id")
        writableDatabase.delete(PLANT_TABLE, "$PLANT_ID = $id", null)
        writableDatabase.delete(WATERING_TABLE, "$WATERING_ID = $id", null)
        writableDatabase.delete(NURTURE_TABLE, "$NURTURE_ID = $id", null)
        writableDatabase.close()
    }

    //WATERING
    fun deleteLatestWatering(plant: Plant): Date {
        Log.d(DB_TAG, "Deleting last watering")
        val wateringsList = readableDatabase.select(WATERING_TABLE).whereArgs(WATERING_ID+" = ${plant.id}").parseList(IntervalParser())
        val sortedList = wateringsList.sortedWith(compareBy({-it.second.time}))
        if(sortedList.size > 1) {
            writableDatabase.delete(WATERING_TABLE, "$WATERING_DATE=?", arrayOf(DateHelper.dateStringFromDate(sortedList[0].second)))
            updateLatestWatering(sortedList[1].second, plant.id)
            writableDatabase.close()
            return sortedList[1].second
        } else {
            Log.d("DB", "Could not delete watering, to few waterings")
            updateLatestWatering(sortedList[0].second, plant.id)
            return sortedList[0].second
        }
    }

    fun water(id: Int, date: Date) {
        Log.d(DB_TAG, "Watering plant")
        writableDatabase.insert(WATERING_TABLE, WATERING_ID to id,
                WATERING_DATE to DateHelper.dateStringFromDate(date))

        updateLatestWatering(date, id)
    }

    //NURTURE
    fun deleteLatestNurture(plant: Plant): Date {
        Log.d(DB_TAG, "Deleting last nurture")
        val nurtureList = readableDatabase.select(NURTURE_TABLE).whereArgs(NURTURE_ID+" = ${plant.id}").parseList(IntervalParser())
        val sortedList = nurtureList.sortedWith(compareBy({-it.second.time}))
        if(sortedList.size > 1) {
            writableDatabase.delete(NURTURE_TABLE, "$NURTURE_DATE=?",  arrayOf(DateHelper.dateStringFromDate(sortedList[0].second)))
            updateLatestNurture(sortedList[1].second, plant.id)
            return sortedList[1].second
        } else {
            Log.d("DB", "Could not delete nurture, to few nurtures")
            updateLatestNurture(sortedList[0].second, plant.id)
            return sortedList[0].second
        }
    }

    fun nurture(id: Int, date: Date) {
        Log.d(DB_TAG, "Nurturing plant")
        writableDatabase.insert(NURTURE_TABLE, NURTURE_ID to id, NURTURE_DATE to DateHelper.dateStringFromDate(date))

        updateLatestNurture(date, id)
    }



    private val PLANT_TABLE = "Plant"
    private val PLANT_ID = "id"
    private val PLANT_NAME = "name"
    private val PLANT_PHOTO = "photo"
    private val PLANT_WATERINGINTERVAL = "wateringInterval"
    private val PLANT_NURTUREINTERVAL = "nurtureInterval"
    private val PLANT_WATERING = "watering"
    private val PLANT_PLACEMENT = "placement"
    private val PLANT_LATESTWATERING = "latestWatering"
    private val PLANT_LATESTNURTURE = "latestNurture"
    private val WATERING_TABLE = "Watering"
    private val WATERING_ID = "wateredPlant"
    private val WATERING_DATE = "wateredDate"
    private val NURTURE_TABLE = "Nurture"
    private val NURTURE_ID = "NurturedPlant"
    private val NURTURE_DATE = "NurturedDate"
    private val DB_TAG = "---DATABASE ACTION---"

    private fun getValuesFromPlant(plant: Plant): ContentValues {
        val plantValues = ContentValues()
        plantValues.put(PLANT_NAME, plant.name)
        plantValues.put(PLANT_PHOTO, plant.imagePath)
        plantValues.put(PLANT_WATERINGINTERVAL, plant.waterInterval)
        plantValues.put(PLANT_NURTUREINTERVAL, plant.nurtureInterval)
        plantValues.put(PLANT_WATERING, PlantHelper.getWaterAmountInt(plant.watering))
        plantValues.put(PLANT_PLACEMENT, PlantHelper.getPlacementInt(plant.placement))

        return plantValues
    }
}
