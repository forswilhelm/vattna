/**
 * Created by wilhelm on 2017-10-20.
 */

enum class watering {
    Rikligt, Måttligt, Sparsamt
}

enum class placement {
    Ljust, Halvljust, Mörkt
}