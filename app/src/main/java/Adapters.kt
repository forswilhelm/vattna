import helpers.DateHelper
import helpers.ImageHelper
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.vattna.wilhelm.vattna.R
import com.squareup.picasso.Picasso
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread


class RecyclerPlantListAdapter(val context: Context, var plants: List<Plant>, val userClickListener: ClickListener) : RecyclerView.Adapter<RecyclerPlantListAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(plants[position])
    }

    override fun getItemCount(): Int {
        return plants.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.plant_list_cell, parent, false)
        return ViewHolder(view)
    }


    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view), View.OnClickListener {
        override fun onClick(p0: View?) {
            userClickListener.onItemClick(adapterPosition)
        }

        fun bind(plant: Plant) {
            val plantNameLabel = view.findViewById<TextView>(R.id.plant_list_name)
            val lastWaterLabel = view.findViewById<TextView>(R.id.plant_list_last_watered)
            val nextWaterLabel = view.findViewById<TextView>(R.id.plant_list_next_water)
            val plantImage = view.findViewById<ImageView>(R.id.plant_list_image)
            val waterReminder = view.findViewById<ImageView>(R.id.water_reminder)

            val waters = context.getString(R.string.next_water)+": ${DateHelper.dayDateString(plant.daysToWater(), context)}"
            val latestWater = context.getString(R.string.watered)+" ${DateHelper.dayDateString(plant.latestWatering, context)}"
            plantNameLabel.text = plant.name
            lastWaterLabel.text = latestWater
            nextWaterLabel.text = waters
            val visibility: Int
            if(plant.daysToWater() == 0) {
                visibility = View.VISIBLE
            } else {
                visibility = View.GONE
            }
            waterReminder.visibility = visibility

            if(plant.imagePath != null) {
                doAsync {
                    val image = ImageHelper.getCorrectRotation(plant.imagePath, ImageHelper.getOptionsSmall())
                    context.runOnUiThread {
                        Picasso.with(context).load(ImageHelper.getImageUri(context, image)).fit().centerCrop().into(plantImage)
                    }
                }
            } else {
                Picasso.with(context).load(R.drawable.plants).fit().centerCrop().into(plantImage)
            }

            view.setOnClickListener(this)
        }

    }

}

interface ClickListener {
    fun onItemClick(position: Int)
}

